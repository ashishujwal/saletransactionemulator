package main

import (
	"time"
	"math/rand"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)


type SaleProduct struct {
	Quantity int `json:"quantitySold"`
	ProductName string `json:"productName"'`
	ProductId string `json:"productId"`
	CostPrice float64 `json:"costPrice"`
	SellingPrice float64`json:"sellingPrice"`
}

type SaleTransaction struct {
	StoreId string `json:"storeId"`
	StoreName string `json:"storeName"`
	RetailerId string `json:"ownerId"`
	RetailerName string `json:ownerName`
	TerminalId string `json:"terminalId"`
	CustomerId string `json:"customerId"`
	CustomerName string `json:"customerName"`
	SalesExecutiveId string `json:"salesExecutiveName"`
	PaymentMethod string `json:"paymentMethod"`
	TotalAmount float64 `json:"totalAmount"`
	PromoCode string `json:"promoCode"`
	TimeStamp time.Time `json:"timestamp"`
	SaleProducts [] SaleProduct `json:"salesproduct"`
}

type Store struct {
	storeId string
	storeName string
}

var StoreOne = Store{"Store One " , "Store One"}
var StoreTwo = Store{"Store Two", "Store Two"}
var StoreThree = Store{"Store Three", "Store Three"}




var stores [] Store

type Product struct {
	ProductId string
	ProductName string
	CostPrice float64
	SellingPrice float64
}


var P1 = Product{"Moon Rocks", "Moon Rocks", 10.0,  13.0}
var P2 = Product{"Original Skywalker OG", "Original Skywalker OG", 10.0,  11.5}
var P3 = Product{"King Louis XIII", "King Louis XIII", 10.0,  11}
var P4 = Product{"Yoda OG", "Yoda OG", 10.0, 15.0}
var P5 = Product{"White Rhino", "White Rhino", 13.0, 15.0}
var P6 = Product{"Candy Land", "Candy Land", 5.0, 7.0}
var P7 = Product {"Citrus Sap" , "Citrus Sap", 5, 6.5}
var P8 = Product{"Cat Piss", "Cat Piss", 3.0, 4.0}
var P9 = Product{"Battery Cell" , "Battery Cell", 0.40, 0.50}

var MIN_QTY = 2
var MAX_QTY = 5


func random(min, max int) int {
	nanos :=  time.Now().Nanosecond()
	rand.Seed(int64(nanos))
	return rand.Intn(max - min) + min
}

//var rabbitUrl = "amqp://guest:guest@192.168.2.14:5672/"
var rabbitUrl = "amqp://radmin:radmin@13.127.202.129:5672/"
var exchange = "blockChainEvents"

func main() {
	setup()

	connection, channel := connectWithRabbit()
	defer  connection.Close()
	defer channel.Close()
	i := 0
	for  i < 10000 {
		i++
		saleTransaction := SaleTransaction{}
		customer :=  randomCustomer()
		product := randomProduct()
		store := randomStore()

		saleTransaction.CustomerId = customer
		saleTransaction.CustomerName = customer
		saleTransaction.PaymentMethod = "CASH"
		saleTransaction.PromoCode = "1234"

		randomDays := random(1,30)

		saleTransaction.TimeStamp = time.Now().AddDate(0,0,randomDays)





		saleTransaction.RetailerId = "R1"
		saleTransaction.RetailerName = "R1"
		saleTransaction.StoreId = store.storeId
		saleTransaction.StoreName = store.storeName


		sale := SaleProduct{}
		sale.ProductId = product.ProductId
		sale.ProductName = product.ProductName
		sale.Quantity = random (1,10)
		sale.CostPrice = product.CostPrice
		sale.SellingPrice = product.SellingPrice

		saleTransaction.TotalAmount = float64(sale.Quantity) * sale.SellingPrice

		var productSold [] SaleProduct

		productSold = append(productSold, sale)
		saleTransaction.SaleProducts = productSold

		b, e := json.Marshal(saleTransaction)
		if e != nil {
			continue
		} else {
			shouldPublish := true
			if shouldPublish {
				channel.Publish(
					exchange,
					"",
					false,
					false,
					amqp.Publishing{
						ContentType: "application/application/json",
						Body:b,
					},
				)
			}

		}

		log.Println("Published %s" , string(b))

	}
}

func connectWithRabbit() (*amqp.Connection, *amqp.Channel) {
	outBoundConn, err := amqp.Dial(rabbitUrl)
	if err != nil {
		panic("Could not open connection with rabbit.")
	}

	outboundChannel, err := outBoundConn.Channel()
	if err != nil {
		panic("Failed to open outbound channel to rabbit MQ." )
	}


	err = outboundChannel.ExchangeDeclare(
		exchange, // name
		"fanout",                             // type
		true,                                 // durable
		false,                                // auto-deleted
		false,                                // internal
		false,                                // no-wait
		nil,                                  // arguments
	)

	return outBoundConn, outboundChannel
}

func randomCustomer() string {
	return customers.Names[random(0, len(customers.Names))]
}

func randomProduct() Product {
	return products[random(0,len(products))]
}

func randomStore() Store {
	return stores[random(0,len(stores))]
}

type CustomerNames struct {
	Names [] string
}

var products [] Product
var customers CustomerNames

func setup() {
	stores = append(stores, StoreOne)
	stores = append(stores, StoreTwo)
	stores = append(stores, StoreThree)


	products = append(products, P1)
	products = append(products, P2)
	products = append(products, P3)
	products = append(products, P4)
	products = append(products, P5)
	products = append(products, P6)
	products = append(products, P7)
	products = append(products, P8)
	products = append(products, P9)

	b, err := ioutil.ReadFile("customerNames.json")
	if err != nil {
		//panic("Could not read the file.")
		fmt.Println("errror occurred %v" , err.Error())
	}

	json.Unmarshal(b,&customers)
	fmt.Println("Number of customers ... %d" , len(customers.Names))


}