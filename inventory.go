package main

import "time"

type inventoryUpdate struct {
	StoreId string `json:"storeId"`
	StoreName string `json:"storeName"`
	OwnerId string `json:"ownerId"`
	OwnerName string `json:"ownerName"`
	TimeStamp time.Time `json:"timestamp"`
	ProductId string `json:"productId"`
	ProductName string `json:"productName"`
	ProductInventoryId string `json:"productInventoryId"`
}